﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClassEvent
{
    public abstract class Shape
    {
        protected double area;
        public double Area
        {
            get { return area; }
            set { area = value; }
        }

        public EventHandler<ShapeEventArgs> ShapeChanged;

        public abstract void Draw();

        protected virtual void OnShapeChanged(ShapeEventArgs e)
        {
            Console.WriteLine("OnShapeChanged in Shape");

            EventHandler<ShapeEventArgs> handler = ShapeChanged;

            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
