﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClassEvent
{
    public class Circle:Shape
    {
        private double radius;
        public Circle(double d)
        {
            radius = d;
            area = d * d * 3.14;
        }

        public void Update(double d)
        {
            radius = d;
            area = 3.14 * radius * radius;
            OnShapeChanged(new ShapeEventArgs(area));
        }

        protected override void OnShapeChanged(ShapeEventArgs e)
        {
            Console.WriteLine("OnShapeChanged in circle");

            // Call the base class event invocation method.
            base.OnShapeChanged(e);
        }
        public override void Draw()
        {
            Console.WriteLine("Drawing a circle");
        }
    }
}
