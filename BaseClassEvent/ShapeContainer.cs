﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClassEvent
{
    public class ShapeContainer
    {
        List<Shape> _list;
        object _locker;

        public ShapeContainer()
        {
            _list = new List<Shape>();
            _locker = new object();
        }

        public void AddShape(Shape s)
        {
            lock(_locker)
            {
                _list.Add(s);
                s.ShapeChanged += HandlerShapeChanged;
            }
        }

        private void HandlerShapeChanged(object sender, ShapeEventArgs e)
        {
            Shape shape = (Shape)sender;

            Console.WriteLine("Receive event . ShapeArea is now {0}", shape.Area);

            shape.Draw();
        }
    }
}
